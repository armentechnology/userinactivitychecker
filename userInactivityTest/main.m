//
//  main.m
//  userInactivityTest
//
//  Created by Jose Catala on 06/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//
// Added  UserActivityChecker UIApplication subclass to check user inactivity after a time

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "UserActivityChecker.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, NSStringFromClass([UserActivityChecker class]), NSStringFromClass([AppDelegate class]));
    }
}
