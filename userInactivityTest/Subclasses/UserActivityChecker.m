//
//  UserActivityChecker.m
//  userInactivityTest
//
//  Created by Jose Catala on 06/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "UserActivityChecker.h"

@interface UserActivityChecker()
{
    NSTimer *myidleTimer;
}
@end

@implementation UserActivityChecker

- (id) init
{
    self = [super init];
    
    if (self)
    {
        [self resetIdleTimer];
    }
    
    return self;
}

#pragma  mark OVERRIDE CLASS

/* listening for any touch, if received the timer is reset */
-(void)sendEvent:(UIEvent *)event
{
    [super sendEvent:event];
    
    if (!myidleTimer)
    {
        [self resetIdleTimer];
    }
    
    NSSet *allTouches = [event allTouches];
    if ([allTouches count] > 0)
    {
        UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
        if (phase == UITouchPhaseBegan)
        {
            [self resetIdleTimer];
        }
    }
}

#pragma mark PRIVATE METHODS

-(void)resetIdleTimer
{
    if (myidleTimer)
    {
        [myidleTimer invalidate];
    }
    int timeout = kApplicationTimeoutInSecons;
    
    myidleTimer = [NSTimer scheduledTimerWithTimeInterval:timeout target:self selector:@selector(idleTimerExceeded:) userInfo:nil repeats:NO];
    
    myidleTimer.tolerance = timeout * 0.1;
}

/* if the timer reaches the limit post the notification */
-(void)idleTimerExceeded:(NSTimer *)timer
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kApplicationDidTimeoutNotification object:nil];
    
    [timer invalidate];
}

@end
