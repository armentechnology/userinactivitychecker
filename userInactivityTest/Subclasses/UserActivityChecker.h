//
//  UserActivityChecker.h
//  userInactivityTest
//
//  Created by Jose Catala on 06/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kApplicationTimeoutInSecons 5

#define kApplicationDidTimeoutNotification @"SessionExpired"

@interface UserActivityChecker : UIApplication


@end
